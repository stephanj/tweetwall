package devoxxinfoapp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for loading all session data from devoxx server
 * 
 * @author Jasper Potts
 */
public class DataFetcher {
    private static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Map<Integer,Speaker> speakerMap = new HashMap<>();
    private Map<Integer,Presentation> presentationMap = new HashMap<>();
    private final String appRoom;

    public DataFetcher(String room) {
        this.appRoom = room;
    }
    
    public List<Presentation> getData() {
        System.out.println("");
        // GET ALL SPEAKERS FIRST
        try{
            JSONParserJP.parse("http://cfp.devoxx.com/rest/v1/events/7/speakers",new SpeakerCallcack());
        } catch (Exception e) { e.printStackTrace(); }
        System.out.println("");
        // THEN GET ALL Presentation TIMES
        try{
            JSONParserJP.parse("http://cfp.devoxx.com/rest/v1/events/7/schedule ",new SessionTimeCallcack());
        } catch (Exception e) { e.printStackTrace(); }
        System.out.println("");
        
        // THEN GET ALL Presentation EXTRA DATA
        try{
            JSONParserJP.parse("http://cfp.devoxx.com/rest/v1/events/7/presentations",new SessionCallcack());
        } catch (Exception e) { e.printStackTrace(); }
        System.out.println("");
        System.out.println("FOUND ["+speakerMap.size()+"] SPEAKERS");
        System.out.println("FOUND ["+presentationMap.size()+"] PRESENTATIONS");
        System.out.println("");
        
        // SORT PRESENTATIONS BY TIME
        List<Presentation> presentations = new ArrayList<>(presentationMap.values());
        Collections.sort(presentations, new Comparator<Presentation>() {
            @Override public int compare(Presentation o1, Presentation o2) {
                return o1.fromTime.compareTo(o2.fromTime);
            }
        });
        return presentations;
    }
    
    private class SpeakerCallcack extends JSONParserJP.CallbackAdapter {
        private int id;
        private String firstName;
        private String lastName;
        private String company;
        private String bio;
        private String imageUrl;
        private String twitter;
        private boolean rockStar = false;
        
        @Override public void keyValue(String key, String value, int depth) {
            if(depth == 2) {
                if ("id".equals(key)) {
                    id = Integer.parseInt(value);
                } else if ("lastName".equals(key)) {
                    lastName = value;
                } else if ("firstName".equals(key)) {
                    firstName = value;
                } else if ("bio".equals(key)) {
                    bio = value;
                } else if ("company".equals(key)) {
                    company = value;
                } else if ("imageURI".equals(key)) {
                    imageUrl = value;
                    // there is a bug that we can not follow redirects, so switch to https up front. http://javafx-jira.kenai.com/browse/RT-25739
//                    System.out.println("imageUrl = " + imageUrl);
                    if (imageUrl.startsWith("http:")) imageUrl = "https" + imageUrl.substring(4);
//                    System.out.println("   -- >imageUrl = " + imageUrl);
                } else if ("tweethandle".equals(key)) {
                    twitter = value;
                }
            }
        }
        @Override public void endObject(String objectName, int depth) {
            if(depth == 1) {
                Speaker speaker = speakerMap.get(id);
                if (speaker == null) {
                    speaker = new Speaker(id,firstName+" "+lastName,imageUrl);
                    speakerMap.put(id,speaker);
                    System.out.print("\rFound ["+speakerMap.size()+"] Speakers");
                }
            }
        }
    }
    
    private class SessionTimeCallcack extends JSONParserJP.CallbackAdapter {
        public int id;
        private Date start,end;
        private int length;
        private String title;
        private String room;
        private int sessionId;

        @Override public void keyValue(String key, String value, int depth) {
            if(depth == 2) {
                if ("id".equals(key)) {
                    id = Integer.parseInt(value);
                } else if ("presentationUri".equals(key)) {
                    //summary = value;
                    int lastSlash = value.lastIndexOf('/');
                    sessionId = Integer.parseInt(value.substring(lastSlash+1, value.length()));
                } else if ("fromTime".equals(key)) {
                    try {
                        start = DATE_FORMAT.parse(value);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                } else if ("toTime".equals(key)) {
                    try {
                        end = DATE_FORMAT.parse(value);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                } else if ("room".equals(key)) {
                    room = value;
                } else if ("title".equals(key)) {
                    title = value;
                }
            }
        }
        
        @Override public void endObject(String objectName, int depth) {
            if(depth == 1) {
                // first check if its for this room
                if (room.equals(appRoom)) {
                    length = (int)((end.getTime()/60000) - (start.getTime()/60000));
                    presentationMap.put(
                        sessionId,
                        new Presentation(sessionId, title, room, start, end, length)
                    );
                    System.out.print("\rFound ["+presentationMap.size()+"] Presentations");
                }
            }
        }
    }
    
    private class SessionCallcack extends JSONParserJP.CallbackAdapter {
        public int id;
        private String summary;
        private String level;
        private List<Speaker> speakers = new ArrayList<>();
        private String type;
        private String track;
        
        private int count = 0;

        @Override public void keyValue(String key, String value, int depth) {
            if(depth == 2) {
                if ("id".equals(key)) {
                    id = Integer.parseInt(value);
                } else if ("summary".equals(key)) {
                    summary = value;
                } else if ("track".equals(key)) {
                    track = value;
                } else if ("experience".equals(key)) {
                    level = value;
                } else if ("type".equals(key)) {
                    type = value;
                }
            } else if (depth == 4 && "speakerId".equals(key)) {
                speakers.add(speakerMap.get(Integer.parseInt(value)));
            }
        }
        
        @Override public void endObject(String objectName, int depth) {
            if(depth == 1) {
                Presentation presentation = presentationMap.get(id);
                if (presentation != null) {
                    presentation.setExtended(summary, speakers.toArray(new Speaker[speakers.size()]), level, track, type);
                    System.out.print("\rUpdated ["+(++count)+"] Presentations");
                }
            }
        }

        @Override public void startObject(String objectName, int depth) {
            if(depth == 1) {
                speakers.clear();
            }
        }
    }
    
}
