package devoxxinfoapp;

import javafx.scene.image.Image;

/**
 * Model object for conference Speaker.
 * 
 * @author Jasper Potts
 */
public class Speaker {
    public final int id;
    public final String fullName;
    public final String imageUrl;
    public Image image;

    public Speaker(int id, String fullName, String imageUrl) {
        this.id = id;
        this.fullName = fullName;
        this.imageUrl = imageUrl;
    }

    @Override public String toString() {
        return fullName;
    }
}
